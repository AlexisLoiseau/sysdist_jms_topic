package hepl.sysdist.activemq.topic;

public class CustomMessage {

    private String to;
    private String body;
    private Integer monnum;

    public CustomMessage() {
    }

    public CustomMessage(String to, String body, Integer num) {
        this.to = to;
        this.body = body;
        this.monnum = num;

    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getMonnum() {
        return monnum;
    }

    public void setMonnum(Integer monnum) {
        this.monnum = monnum;
    }

    @Override
    public String toString() {
        return String.format("Email{to=%s, body=%s} %d", getTo(), getBody(), getMonnum());
    }

}
