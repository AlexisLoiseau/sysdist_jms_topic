package hepl.sysdist.activemq.topic;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;


@SpringBootApplication
@EnableJms
public class ActivemqApplication {

    public static void main(String[] args) {
        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(ActivemqApplication.class, args);
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
        System.out.println("Sending a message.");
        jmsTemplate.convertAndSend("mytopic", new CustomMessage("info@example.com", "Hello", 2),messagePostProcessor -> {
            messagePostProcessor.setStringProperty("priority",
                    "high");
            return messagePostProcessor;
        });
    }
}
